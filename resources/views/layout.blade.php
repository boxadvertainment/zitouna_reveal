<!doctype html>
<!--[if lt IE 9]>      <html class="no-js lt-ie9" lang="{{ App::getLocale() }}"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="{{ App::getLocale() }}"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title'){{ Config::get('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- <meta name="author" content="box.agency" /> -->

    @include('partials.socialMetaTags', [
        'title' => "5 ans Banque Zitouna",
        'description' => "Banque Zitouna fête ses 5 ans avec vous. 500 DT sont à gagner quotidiennement et ce jusqu'au 30 Septembre."
    ])

    <!-- Meta CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- Place favicon.ico in the root directory -->
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon" >

    <link rel="stylesheet" href="{{ asset('css/vendor.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css?s=2') }}">

    @yield('styles')

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        BASE_URL    = '{{ url('/') }}';
        CURRENT_URL = '{{ Request::url() }}';
        FB_APP_ID   = '{{ config('services.facebook.client_id') }}';
    </script>
    <script src="{{ asset('js/modernizr.js') }}"></script>
</head>
<body class="@yield('class')">

    <div class="loader">
        <div class="spinner">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>

    <!--[if lt IE 9]>
    <div class="alert alert-dismissible outdated-browser show" role="alert">
        <h6>Votre navigateur est obsolète !</h6>
        <p>Pour afficher correctement ce site et bénéficier d'une expérience optimale, nous vous recommandons de mettre à jour votre navigateur.
            <a href="http://outdatedbrowser.com/fr" class="update-btn" target="_blank">Mettre à jour maintenant </a>
        </p>
        <a href="#" class="close-btn" title="Fermer" data-dismiss="alert" aria-label="Fermer">&times;</a>
    </div>
    <![endif]-->

<div class="app-wrapper">

    <header class="header">
      <div class="inner-wrapper clearfix">
        <div class="social-links">
          <a href="https://www.facebook.com/banque.zitouna.tn" target="_blank" class="fb-share-btn"><i class="fa fa-facebook"></i></a>
          <a href="https://www.youtube.com/user/banquezitouna" target="_blank" class="tw-share-btn"><i class="fa fa-youtube"></i></a>
          <a href="https://twitter.com/Banque_Zitouna" target="_blank" class="tw-share-btn"><i class="fa fa-twitter"></i></a>
          <a href="http://www.banquezitouna.com/Fr/accueil_46_7" target="_blank" class="fb-share-btn"><i class="fa fa-globe"></i></a>
          <a href="#" target="_blank" class="fb-share-btn" id="sound-ctrl"><i class="fa fa-volume-up"></i></a>
        </div>
      </div>
    </header>

    <main class="main" role="main">
        <div class="inner-wrapper clearfix">
            <div class="left-side">

              <a href="{{ url('/') }}"><img src="img/aniv-logo.png" alt="5ans Zitouna"></a>

              <div class="btn-wrapper">
                  @if (Request::route()->getUri() == 'expert')
                      <a href="{{ url('/') }}" class="btn">{{ $closed ? 'Accueil' : 'Le  Quizz' }}</a>
                  @else
                      <div id="expert-sprite"></div>
                      <a href="{{ url('expert') }}" class="btn">Notre expert</a>
                  @endif

                  @if (Request::route()->getUri() == 'historique')
                      <a href="{{ url('/') }}" class="btn">{{ $closed ? 'Accueil' : 'Le  Quizz' }}</a>
                  @else
                      <a href="{{ url('historique') }}" class="btn">Nos 5 ans</a>
                  @endif

                <p class="copyright">
                <a href="{{ asset('reglement.pdf') }}" target="_blank"> Règlement du Jeu</a> <br>
                Tous droits réservés © 2015 Banque Zitouna</p>
              </div>
            </div>

            <div class="right-side">
                <div class="content">
                    @yield('content')
                </div>
            </div>
        </div>

        <div class="video-bg">
          <div class="overlay-video"></div>
          <video id="video" class="video video-js" preload="auto" width="100%" height="100%"></video>
          <video id="video-quiz" class="video video-js" preload="auto" width="100%" height="100%"></video>
        </div>

        </main>

    </div>

    @yield('body')

    <div class="modal fade" id="form-modal">
      <div class="modal-dialog">
        <div class="modal-content">

          <form id="signup-form" action="{{ action('AppController@signup') }}" method="post">
              {!! csrf_field() !!}
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" id="name" maxlength="30" placeholder="Nom *" required autofocus  title="Ce champ est obligatoire">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <input type="text" class="form-control" name="lastname" id="lastname" maxlength="30" placeholder="Prénom *" required  title="Ce champ est obligatoire">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email *" required  title="Email incorrect">
                    </div>
                </div>
                <div class="col-xs-6">
                    <div class="form-group">
                        <input type="text" class="form-control" name="phone" id="phone"  maxlength="8" placeholder="Téléphone *" required pattern="^[2549][0-9]{7}$" title="Téléphone incorrect">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-xs-offset-3">
                    <div class="form-group">
                        <input type="text" class="form-control" name="cin" id="cin" maxlength="8" placeholder="CIN *" required  pattern="^[0-9]{8}$" title="CIN incorrect">
                    </div>
                </div>
            </div>

              <div class="form-group text-center">
                  <p class="info">En cliquant sur "Validez", vous acceptez le <a href="{{ asset('reglement.pdf') }}" target="_blank">Règlement du jeu</a></p>
              </div>

              <button type="submit" class="btn btn-next">Valider</button>
          </form>

        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->


    <script src="{{ asset('js/vendor.js') }}"></script>
    <script src="{{ asset('js/main.js?s=2') }}"></script>

    @yield('scripts')

    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-67512881-2', 'auto');
      ga('send', 'pageview');

    </script>
</body>
</html>
