@extends('layout')

@section('class', 'expert')

@section('content')
    <div class="page expert-page">
        <iframe id="expert-content" name="expert-content"></iframe>
        <div class="expert-video-wrapper">
            <div class="overlay"></div>
            <video id="expert-video" class="video-js" preload="auto" width="720px" height="405px" data-setup='{ "techOrder": ["youtube"], "src": "http://www.youtube.com/watch?v=z5JH1mANcD8", "quality":"1080p" }'></video>
        </div>
        <div class="social-footer"></div>
        <div class="btns-services">
            <a target="expert-content" href="http://www.eddar.tn?src=reveal" class="service-btn dar"></a>
            <a target="expert-content" href="http://www.elkarhba.tn?src=reveal" class="service-btn car"></a>
            <a target="expert-content" href="http://www.el3omra.tn?src=reveal" class="service-btn omra"></a>
            <a target="expert-content" href="http://www.elmosta9bel.tn?src=reveal" class="service-btn tawfir"></a>
            <a target="expert-content" href="http://www.elwa9t.tn?src=reveal" class="service-btn tawasol"></a>
        </div>
    </div>
@endsection
