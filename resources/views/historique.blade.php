@extends('layout')

@section('class', 'historique')

@section('content')
    <div class="page historique-page">

        <div class="item item-2009" data-toggle="modal" data-target="#historique-modal" data-year="2009">
            <h2>2009</h2>
            <div class="description">
               Démarrage Officiel des Activités...
            </div>
        </div>

        <div class="item item-2010" data-toggle="modal" data-target="#historique-modal" data-year="2010">
            <h2>2010</h2>
            <div class="description">
               Démarrage Officiel des Activités...
            </div>
        </div>

        <div class="item item-2011" data-toggle="modal" data-target="#historique-modal" data-year="2011">
            <h2>2011</h2>
            <div class="description">
                Nomination d’un Administrateur Provisoire...
            </div>
        </div>

        <div class="item item-2012" data-toggle="modal" data-target="#historique-modal" data-year="2012">
            <h2>2012</h2>
            <div class="description">
                Nomination d’un nouveau conseil d’administration...
            </div>
        </div>
        <div class="item item-2013" data-toggle="modal" data-target="#historique-modal" data-year="2013">
            <h2>2013</h2>
            <div class="description">
                Approbation de l’introduction du Partenaire...
            </div>
        </div>
        <div class="item item-2014" data-toggle="modal" data-target="#historique-modal" data-year="2014">
            <h2>2014</h2>
            <div class="description">
                Lancement de l’Activité Western Union...
            </div>
        </div>
        <div class="item item-2015" data-toggle="modal" data-target="#historique-modal" data-year="2015">
            <h2>2015</h2>
            <div class="description">
                Organisation de la 2 ème Edition des Journées Banque...
            </div>
        </div>
    </div>
@endsection

@section('body')
<div id="historique-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>

                <div class="content content-2009">
                    <h2 class="title">Ouverture de la Banque</h2>
                    <div class="description">
                        <ul>
                            <li> <b>Janvier 2009 :</b> Obtention de l’Agrément de Banque Universelle</li>
                            <li> <b>ctobre 2009 :</b> Création Légale de la Banque </li>
                        </ul>
                    </div>
                </div>

                <div class="content content-2010">
                    <h3 class="year">2010</h3>
                    <h2 class="title">Les Faits Marquants</h2>
                    <div class="description">
                        <ul>
                            <li> <b>Mai 2010 :</b> Démarrage Officiel des Activités de la Banque</li>
                            <li> <b>Août 2010 :</b> Augmentation du Capital de 35 MD </li>
                            <li> <b>Décembre 2010 :</b> 20 Agences et 21 DAB</li>
                            
                        </ul>
                    </div>
                </div>

                <div class="content content-2011">
                    <h3 class="year">2011</h3>
                    <h2 class="title">Les Faits Marquants</h2>
                    <div class="description">
                        <ul>
                            <li> <b>Janvier 2011 :</b> Nomination d’un Administrateur Provisoire</li>
                            <li> <b>Mars 2011 :</b> Prise de Contrôle par l’Etat </li>
                            <li> <b>Décembre 2011 :</b> 25 Agences et 26 DAB</li>
                            
                        </ul>
                    </div>
                </div>

                <div class="content content-2012">
                    <h3 class="year">2012</h3>
                    <h2 class="title">Les Faits Marquants</h2>
                    <div class="description">
                        <ul>
                            <li>  <b>Juin 2012 :</b> Nomination d’un nouveau conseil d’administration et d’un nouveau PDG </li>
                            <li>  <b>Juin 2012 :</b> Lancement de l’Activité de BancaTakaful</li>
                            <li>  <b>Décembre 2012 :</b> 35 Agences et 36 DAB </li>
                        </ul>
                    </div>
                </div>
                <div class="content content-2013">
                    <h3 class="year">2013</h3>
                    <h2 class="title">Les Faits Marquants</h2>
                    <div class="description">
                        <ul>
                            <li> <b>Mars 2013 :</b> Approbation de l’introduction du Partenaire Stratégique (la Banque Islamique de Développement) par le Chef du Gouvernement</li>
                            <li> <b>Juin 2013 :</b> Organisation de la 1ère Edition des Journées Banque Zitouna de la Finance Islamique </li>
                            <li> <b>Décembre 2013 :</b> 49 Agences et 50 DAB</li>
                        </ul>
                    </div>
                </div>
                <div class="content content-2014">
                    <h3 class="year">2014</h3>
                    <h2 class="title">Les Faits Marquants</h2>
                    <div class="description">
                        <ul>
                            <li> <b>Mars 2014 :</b> Lancement de l’Activité Western Union</li>
                            <li> <b>Avril 2014 :</b> Signature d’un protocole d’accord stratégique relatif à la coopération bilatérale avec les Banques Islamiques Africaines </li>
                            <li> <b>Décembre 2014 :</b> Augmentation du Capital de 18.5 MD à travers l’Introduction de la BID </li>
                            <li> <b>Décembre 2014 :</b> Renforcement du dispositif de lutte contre le Blanchiment d’argent et le Financement du Terrorisme (LAB/FT) et la loi FATCA</li>
                            <li> <b>Décembre 2014 :</b> 67 Agences et 68 DAB</li>
                        </ul>
                    </div>
                </div>
                <div class="content content-2015">
                    <h3 class="year">2015</h3>
                    <h2 class="title">Les Faits Marquants</h2>
                    <div class="description">
                        <ul>
                            <li> <b>Juin 2015 :</b> Organisation de la 2 ème Edition des Journées Banque Zitouna de la Finance Islamique ;</li>
                            <li> <b>Juin 2015 :</b> Approbation de la Stratégie Zitouna 2020 </li>
                            <li> <b>Juillet 2015 :</b> Accord de Principe pour la constituions de Zitouna Tamkeen</li>
                            <li> <b>Septembre 2015 :</b> Approbation du Business Plan 2016-2020 et du nouvel organigramme de la banque </li>
                            <li> <b>Septembre 2015 :</b> 76 Agences et 77 DAB.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
