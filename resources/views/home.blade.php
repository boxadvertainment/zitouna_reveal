@extends('layout')

@if($closed)
    @section('class', 'closed-page')
    @section('content')
        <div class="page">
            <div class="closed-block"></div>
        </div>
    @endsection
@endif

@section('class', 'home-page')

@section('content')
    <div class="page home">
        <div class="btn-wrapper">
            <button id="start-btn" class="login-btn">Participez</button>
        </div>
        <div class="accueil-block">
            Banque Zitouna fête ses 5 ans avec vous. 500 DT sont à gagner quotidiennement et ce jusqu'au 30 Septembre.
        </div>

    </div>
    <!-- .home page -->

    <div class="page video-links products-page hide">
        <div class="description">
          <h2>Choisissez le produit de votre choix</h2>
          <p>Soyez attentif en écoutant les témoignages de nos clients, ces derniers contiennent des indices qui vous faciliteront votre participation au quizz.</p>
        </div>
        <a href="#"  class="omra product" data-toggle="modal" data-target="#detail-modal" data-video="//www.youtube.com/embed/TfyJOYZEHEw" data-num="2"><img src="img/omra-btn.png"></a>
        <a href="#"  class="karehba product" data-toggle="modal" data-target="#detail-modal" data-video="//www.youtube.com/embed/N1enChFK5Lw" data-num="0"><img src="img/karehba-btn.png"></a>
        <a href="#"  class="manzel product" data-toggle="modal" data-target="#detail-modal" data-video="//www.youtube.com/embed/_JDELaFKtWU" data-num="1"><img src="img/dar-btn.png"></a>
        <a href="#"  class="tawasol product" data-toggle="modal" data-target="#detail-modal" data-video="//www.youtube.com/embed/DnY95C5vyo4" data-num="4"><img src="img/tawasol-btn.png"></a>
        <a href="#"  class="tawfir product" data-toggle="modal" data-target="#detail-modal" data-video="//www.youtube.com/embed/tfeOytFaXgY" data-num="3"><img src="img/tawfir-btn.png"></a>
    </div>

    <div class="page game-page row hide">
        <div class="quiz-wrapper">
            <div class="question-holder">
                <div class="number"></div>
                <h2 id="question"></h2>
            </div>

            <ul id="answers" class="clearfix"></ul>
            <div id="expert-quiz">
                <a href="{{ url('expert') }}" class="btn btn-success" target="_blank">Besoin d'aide?</a>
            </div>
            <div class="progress-wrap">
                <div id="progress-container">
                  {{-- <div class="line"></div> --}}
                  <ul>
                    <!-- Add or remove steps as you please -->
                    <li class="active">1<p>سؤال 1</p></li>
                    <li>2<p>سؤال 2</p></li>
                    <li>3<p>سؤال 3</p></li>
                    <li>4<p>سؤال 4</p></li>
                    <li>5<p>سؤال 5</p></li>
                  </ul>
                </div>
            </div>
        </div>
    </div>


    <div class="page final-page row hide">
        <div class="wrapper">
            <h1 class="win">Félicitation</h1>
            <div class="description win">
                Bravo vous avez répondu correctement aux 5 questions !<br>
                Un tirage au sort annoncera le gagnant sur la page facebook <a href="https://www.facebook.com/banque.zitouna.tn" target="_blank" class="fb-btn"><i class="fa fa-facebook"></i>&nbsp;&nbsp;Banque Zitouna</a>
            </div>
            <div class="smiley smile win"></div>

            <h1 class="lose">Dommage</h1>
            <div class="description lose">
                Vous n'avez pas répondu correctement aux 5 questions mais vous pouvez tenter votre chance dans 24H.<br>
                Voici les mauvaises réponses pour mieux réussir la prochaine fois:
                <ul class="wrong-answers"></ul>
            </div>
            <div class="smiley lose"></div>
        </div>
    </div>
@endsection

@section('body')
<div id="detail-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog bounceIn">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h2 class="title"></h2>
                <div class="">
                    <iframe class="embed-responsive-item" width="100%" height="365px" src="" frameborder="0" allowfullscreen></iframe>
                </div>

                <button type="button" class="btn btn-success start-quiz"></button>
            </div>
        </div>
    </div>
</div>
@endsection
