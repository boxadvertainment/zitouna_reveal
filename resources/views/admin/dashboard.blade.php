<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Deyson Bejarano">
    <title>Admin Panel - 5ans Zitouna</title>

    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

<body>
<div id="snippetContent" style="padding-top:10px;">
    <div class="container">
        <div class="row">
            <div class="col-sm-10"><h1>5ans Zitouna</h1></div>
        </div>
        <hr>
        <div class="alert alert-info" role="alert">
            Le nombre total des participants : <strong>{{ $count }}</strong> <br/>
            Le nombre total des gagnants : <strong>{{ $countWinners }}</strong>
        </div>
        <br/>
        <div class="row">
            <div class="col-sm-3">
                <ul class="list-group">
                    <li class="list-group-item list-group-item-warning"><strong>Nombre Participants / Gagnants par jour: </strong></li>
                    @foreach ($stat as $date => $value)
                        <li class="list-group-item"><span class="badge" style="background-color: rgba(0, 149, 255, 0.84)">{{ $value['gagnants'] }}</span> <span class="badge" style="background-color: rgba(255, 94, 0, 0.84)">{{ $value['participants'] }}</span> {{ $date }}</li>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm-9">
                <table class="table">
                    <thead>
                        <th>#</th>
                        <th>Nom et prénom</th>
                        <th>email</th>
                        <th>Téléphone</th>
                        <th>CIN</th>
                        <th>Date de création</th>
                    </thead>
                    <tbody>
                    @foreach($participants as $key => $participant)
                        <tr @if($participant->winner) class="success" @endif>
                            <td>{{ ($participants->currentPage() - 1) * 10 + $key + 1}}</td>
                            <td><a href="https://www.facebook.com/{{ $participant->facebook_id }}" target="_blank">
                                <img src="http://graph.facebook.com/{{ $participant->facebook_id }}/picture?width=100" class="img-circle" width="30px" height="30px" alt=""/> {{ $participant->name . ' ' .  $participant->lastname}}</a></td>
                            <td>{{ $participant->email }}</td>
                            <td>{{ $participant->phone }}</td>
                            <td>{{ $participant->cin }}</td>
                            <td>{{ $participant->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>


            <div class="text-center">
                {!! $participants->appends(['username' => 'zitouna'])->render() !!}
            </div>

        </div><!--/col-9-->
    </div><!--/row-->
    <style type="text/css">
        body{margin-top:20px;}
    </style>

</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="{{ asset('js/jquery.min.js') }}"><\/script>')</script>
<!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script> -->

</body>
</html>
