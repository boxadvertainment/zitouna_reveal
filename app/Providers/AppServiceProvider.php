<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $shutdownTime = new \DateTime('2015-10-01');

        $closed = false;
        if (new \DateTime() >= $shutdownTime) {
            $closed = true;
        }
        view()->share('closed', $closed);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
