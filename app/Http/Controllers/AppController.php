<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Participant;

class AppController extends Controller
{
    public function home(Request $request)
    {
        return view('home');
    }

    public function expert()
    {
        return view('expert');
    }

    public function historique()
    {
        return view('historique');
    }

    public function signup(Request $request)
    {
        $validator = \Validator::make( $request->all(), [
            'name' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:participants',
            'phone' => 'regex:/^[0-9]{8}$/|unique:participants',
            'cin' => 'required|regex:/^[0-9]{8}$/|unique:participants'
        ]);

        $validator->setAttributeNames([
            'name' => 'Nom',
            'lastname' => 'Prénom',
            'email' => 'Email',
            'phone' => 'Téléphone',
            'cin' => 'CIN',
        ]);

        if ( $validator->fails() ) {
            return response()->json(['success' => false, 'message' => implode('\n', $validator->errors()->all()) ]);
        }

        $participant = session('participant');
        if ( ! $participant ) {
            return response('Votre session a été expirée, veuillez réessayer.', 500);
        }

        $participant->name = $request->input('name');
        $participant->lastname = $request->input('lastname');
        $participant->email = $request->input('email');
        $participant->phone = $request->input('phone');
        $participant->cin = $request->input('cin');
        $participant->ip = $request->ip();

        if ( $participant->save() ) {
            session(['participantId' => $participant->id]);
            return response()->json(['success' => true]);
        }
        return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
    }

    public function loadQuiz($num)
    {
        if (!session('participantId'))
            return response('Votre session a été expirée, veuillez réessayer.', 500);

        $participant = Participant::find( session('participantId') );

        if (in_array($participant->id, [867])) {
            return response()->json(['hasWon' => true]);
        }

        if ($participant->games()->where('created_at', '>', new \DateTime('-1 day'))->first()) {
            if ($participant->winner) {
                return response()->json(['hasWon' => true]);
            }
            return response()->json(['alreadyPlayed' => true]);
        }

        $quiz = array_map(function($array){
			return array_except($array, 'correctAnswer');
		}, config('app.quiz.' . $num));
        session(['quizNum' => $num, 'beganOn' => new \DateTime]);
        return response()->json($quiz);
    }

    public function checkResponse(Request $request)
	{
        if (!session('participantId'))
            return response('Votre session a été expirée, veuillez réessayer.', 500);

        $participant = Participant::find( session('participantId') );
        $beganOn = session('beganOn');
        if (!$beganOn) {
            $participant->cheater = true;
            $participant->save();
            return response('Une erreur s\'est produite, veuillez réessayer ultérieurement.', 500);
        }

        $game = new \App\Game;
        $game->quiz_num = session('quizNum');
        $game->response = collect($request->input('response'))->toJson();
        $game->elapsed_time = (new \DateTime)->getTimestamp() - $beganOn->getTimestamp();
        $participant->games()->save($game);

        $correctAnswers = array_pluck(config('app.quiz.' . session('quizNum')), 'correctAnswer');
        $wrongAnswers = array_diff_assoc($request->input('response'), $correctAnswers);

        $participant->winner = (count($wrongAnswers) == 0) ? true : false;
        $participant->save();

        if ($participant->winner) {
            return response()->json(['success' => true]);
        } else {
            return response()->json(['success' => false, 'wrongAnswers' => $wrongAnswers]);
        }
	}
}
