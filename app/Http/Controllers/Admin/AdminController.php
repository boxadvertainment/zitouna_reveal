<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Participant;

class AdminController extends Controller
{
    public function dashboard(Request $request)
    {
        if ($request->input('username') != 'zitouna') {
            return response('Unauthorized.', 401);
        }

        $participants = Participant::paginate(15);
        $count = Participant::count();
        $countWinners = Participant::where('winner', true)->count();

        $startDate = new \DateTime('21-09-2015');
        while ($startDate < (new \DateTime)) {
            $endDate = clone $startDate;
            $interval = [$startDate, $endDate->modify('+1 day')];
            $query = Participant::whereBetween('updated_at', $interval);
            $stat[$startDate->format('d/m/Y')]['participants'] = Participant::whereBetween('created_at', $interval)->count();
            $stat[$startDate->format('d/m/Y')]['gagnants'] = Participant::whereBetween('updated_at', $interval)->where('winner', true)->count();
            $startDate->modify('+1 day');
        }

        return view('admin.dashboard', ['participants' => $participants, 'count' => $count, 'countWinners' => $countWinners, 'stat' => $stat]);
    }

    public function login()
    {
        return true;
    }

}
