<?php

return [

    /*
	|--------------------------------------------------------------------------
	| General configuration
	|--------------------------------------------------------------------------
	*/

	// Title of the site
	'name' => '5 ans - Banque Zitouna',

	// This address is used for admin purposes, like notification from contact form
	'email' => 'admin@box.agency',

	// Static global variables
	'quiz' => [
        [
            [
    			'question' => 'Tamouil Sayara vous permet de financer des voitures...',
    			'answers' => [
                    'D’occasion uniquement',
                    'Neuves uniquement',
                    'Neuves et d’occasion ne dépassant pas les 3 ans'
    			],
                'correctAnswer' => 2
    		],
            [
    			'question' => 'Quelle est la durée de remboursement maximale pour Tamouil Sayara ?',
    			'answers' => [
                    '<span style="font-size:16px">3 ans</span>',
                    '<span style="font-size:16px">5 ans</span>',
                    '<span style="font-size:16px">7 ans</span>'
    			],
                'correctAnswer' => 2
    		],
            [
    			'question' => 'Quel est le plafond du financement "Tamouil Sayara" ?',
    			'answers' => [
                    '<span style="font-size:16px">50.000 TND</span>',
                    '<span style="font-size:16px">70.000 TND</span>',
                    '<span style="font-size:16px">100.000 TND</span>'
    			],
                'correctAnswer' => 2
    		],
            [
    			'question' => 'Pour les voitures dont la puissance fiscale dépasse les 4 chevaux, Banque Zitouna peut vous financer jusqu’à...',
    			'answers' => [
                    '<span style="font-size:18px">100%</span>',
                    '<span style="font-size:18px">80%</span>',
                    '<span style="font-size:18px">60%</span>'
    			],
                'correctAnswer' => 2
    		],
            [
    			'question' => 'Sur quelle technique se base Tamouil Sayara ?',
    			'answers' => [
                    '<span style="font-size:16px">Mourabaha</span>',
                    '<span style="font-size:16px">Vente de services « بيع خدمات »</span>',
                    '<span style="font-size:16px">Moudharaba</span>'
    			],
                'correctAnswer' => 0
    		]
        ],

        [
            [
    			'question' => 'Tamouil Menzel vous permet d’acheter une maison...',
    			'answers' => [
                    '<span style="font-size:16px">Première main uniquement</span>',
                    '<span style="font-size:16px">Deuxième main uniquement</span>',
                    '<span style="font-size:16px">Première et  deuxième main</span>'
    			],
                'correctAnswer' => 2
    		],
            [
    			'question' => 'Quelle est la durée de remboursement maximale pour Tamouil Menzel ?',
    			'answers' => [
                    '<span style="font-size:16px">7 ans</span>',
                    '<span style="font-size:16px">10 ans</span>',
                    '<span style="font-size:16px">15 ans</span>'
    			],
                'correctAnswer' => 2
    		],
            [
    			'question' => 'Pour Tamouil Menzel, Banque Zitouna peut vous financer jusqu’à...',
    			'answers' => [
                    '<span style="font-size:16px">100%</span>',
                    '<span style="font-size:16px">70%</span>',
                    '<span style="font-size:16px">80%</span>'
    			],
                'correctAnswer' => 2
    		],
            [
    			'question' => 'Tout au long de la période de remboursement, La marge de profit est...',
    			'answers' => [
                    '<span style="font-size:16px">Fixe</span>',
                    '<span style="font-size:16px">Variable</span>',
                    'Partie Fixe et une Partie Variable'
    			],
                'correctAnswer' => 0
    		],
            [
    			'question' => 'Sur quelle technique se base Tamouil Menzel ?',
    			'answers' => [
                    '<span style="font-size:16px">Mourabaha</span>',
                    '<span style="font-size:16px">Vente de services « بيع خدمات »</span>',
                    '<span style="font-size:16px">Moudharaba</span>'
    			],
                'correctAnswer' => 0
    		]
        ],

        [
            [
    			'question' => 'Tamouil Omra vous permet de financer votre voyage à travers...',
    			'answers' => [
                    'Les agences de voyages partenaires',
                    'Toutes les agences de voyages'
    			],
                'correctAnswer' => 0
    		],
            [
    			'question' => 'Quelle est la durée de remboursement maximale pour Tamouil Omra ?',
    			'answers' => [
                    '<span style="font-size:16px">1 an</span>',
                    '<span style="font-size:16px">3 ans</span>',
                    '<span style="font-size:16px">5 ans</span>'
    			],
                'correctAnswer' => 1
    		],
            [
    			'question' => 'Banque Zitouna peut vous financer jusqu’à',
    			'answers' => [
                    '<span style="font-size:16px">80%</span>',
                    '<span style="font-size:16px">70%</span>',
                    '<span style="font-size:12px; margin-top:-10px;display: block;">A la hauteur des dépenses engagées avec une avance de 250 TND par voyage Omra</span>'
    			],
                'correctAnswer' => 2
    		],
            [
    			'question' => 'Quel est le plafond du produit « Tamouil Omra » ?',
    			'answers' => [
                    '<span style="font-size:16px">5.000 TND</span>',
                    '<span style="font-size:16px">10.000 TND</span>',
                    '<span style="font-size:16px">15.000 TND</span>'
    			],
                'correctAnswer' => 2
    		],
            [
    			'question' => 'Sur quelle technique se base Tamouil Omra ?',
    			'answers' => [
                    '<span style="font-size:16px">Mourabaha</span>',
                    '<span style="font-size:16px">Vente de services « بيع خدمات »</span>',
                    '<span style="font-size:16px">Ijara</span>'
    			],
                'correctAnswer' => 1
    		]
        ],

        [
            [
    			'question' => 'Avec la carte Tawfir vous pouvez retirer de l’argent...',
    			'answers' => [
                    'Des distributeurs Banque Zitouna uniquement',
                    '<span style="font-size:13px">Des distributeurs Banque Zitouna et ceux des banques consœurs</span>',
                    'Des distributeurs des banques consœurs uniquement'
    			],
                'correctAnswer' => 1
    		],
            [
    			'question' => 'Quelle est la technique utilisée pour la rémunération du compte Tawfir ?',
    			'answers' => [
                    '<span style="font-size:16px">Moudharaba</span>',
                    '<span style="font-size:16px">Ijara</span>',
                    '<span style="font-size:16px">Mourabaha</span>'
    			],
                'correctAnswer' => 0
    		],
            [
    			'question' => 'Quel est le montant minimum pour chaque opération de retrait ?',
    			'answers' => [
                    '<span style="font-size:18px">10 TND</span>',
                    '<span style="font-size:18px">20 TND</span>',
                    '<span style="font-size:18px">50 TND</span>'
    			],
                'correctAnswer' => 0
    		],
            [
    			'question' => 'La rémunération au niveau du Hisseb Tawfir est une rémunération versée...',
    			'answers' => [
                    '<span style="font-size:16px">Chaque année</span>',
                    '<span style="font-size:16px">Chaque semestre</span>',
                    '<span style="font-size:16px">Chaque trimestre</span>'
    			],
                'correctAnswer' => 2
    		],
            [
    			'question' => 'La carte Tawfir vous permet de...',
    			'answers' => [
                    '<span style="font-size:16px">Retirer de l’argent</span>',
                    '<span style="font-size:16px">Effectuer des paiements</span>',
                    '<span style="font-size:16px">Acheter à l’international</span>'
    			],
                'correctAnswer' => 0
    		]
        ],

        [
            [
    			'question' => 'Le service Tawassol Mobile (Light) vous permet de...',
    			'answers' => [
                    'Demander un chéquier',
                    'Faire des virements entre vos comptes',
                    'Faire des virements vers des tiers bénéficiaires'
    			],
                'correctAnswer' => 0
    		],
            [
    			'question' => 'Tawassol Pro vous permet d’effectuer des virements...',
    			'answers' => [
                    'Entre vos comptes seulement',
                    'Entre vos comptes et envers des tiers bénéficiaires',
                    'Envers des comptes à l’international'
    			],
                'correctAnswer' => 1
    		],
            [
    			'question' => 'Tawassol Internet vous permet de...',
    			'answers' => [
                    'Télécharger vos extraits de comptes',
                    'Faire la simulation de vos financements',
                    'Entrée en relation à distance'
    			],
                'correctAnswer' => 0
    		],
            [
    			'question' => 'Le service SMS Banking vous permet de connaitre...',
    			'answers' => [
                    'Les 2 dernières opérations',
                    'Les 3 dernières opérations',
                    'Les 5 dernières opérations'
    			],
                'correctAnswer' => 1
    		],
            [
    			'question' => 'Quelle est la formule Tawassol qui vous permet de faire des simulations de vos financements ?',
    			'answers' => [
                    '<span style="font-size:16px">Internet Banking</span>',
                    '<span style="font-size:16px">Mobile Banking</span>',
                    '<span style="font-size:16px">SMS Banking</span>'
    			],
                'correctAnswer' => 1
    		]
        ]
	],

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'debug' => env('APP_DEBUG', false),

    /*
    |--------------------------------------------------------------------------
    | Application URL
    |--------------------------------------------------------------------------
    |
    | This URL is used by the console to properly generate URLs when using
    | the Artisan command line tool. You should set this to the root of
    | your application so that it is used when running Artisan tasks.
    |
    */

    'url' => 'http://localhost',

    /*
    |--------------------------------------------------------------------------
    | Application Timezone
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default timezone for your application, which
    | will be used by the PHP date and date-time functions. We have gone
    | ahead and set this to a sensible default for you out of the box.
    |
    */

    'timezone' => 'Africa/Tunis',

    /*
    |--------------------------------------------------------------------------
    | Application Locale Configuration
    |--------------------------------------------------------------------------
    |
    | The application locale determines the default locale that will be used
    | by the translation service provider. You are free to set this value
    | to any of the locales which will be supported by the application.
    |
    */

    'locale' => 'fr',

    /*
    |--------------------------------------------------------------------------
    | Application Fallback Locale
    |--------------------------------------------------------------------------
    |
    | The fallback locale determines the locale to use when the current one
    | is not available. You may change the value to correspond to any of
    | the language folders that are provided through your application.
    |
    */

    'fallback_locale' => 'en',

    /*
    |--------------------------------------------------------------------------
    | Encryption Key
    |--------------------------------------------------------------------------
    |
    | This key is used by the Illuminate encrypter service and should be set
    | to a random, 32 character string, otherwise these encrypted strings
    | will not be safe. Please do this before deploying an application!
    |
    */

    'key' => env('APP_KEY', 'SomeRandomString'),

    'cipher' => 'AES-256-CBC',

    /*
    |--------------------------------------------------------------------------
    | Logging Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log settings for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Settings: "single", "daily", "syslog", "errorlog"
    |
    */

    'log' => 'single',

    /*
    |--------------------------------------------------------------------------
    | Autoloaded Service Providers
    |--------------------------------------------------------------------------
    |
    | The service providers listed here will be automatically loaded on the
    | request to your application. Feel free to add your own services to
    | this array to grant expanded functionality to your applications.
    |
    */

    'providers' => [

        /*
         * Laravel Framework Service Providers...
         */
        Illuminate\Foundation\Providers\ArtisanServiceProvider::class,
        Illuminate\Auth\AuthServiceProvider::class,
        Illuminate\Broadcasting\BroadcastServiceProvider::class,
        Illuminate\Bus\BusServiceProvider::class,
        Illuminate\Cache\CacheServiceProvider::class,
        Illuminate\Foundation\Providers\ConsoleSupportServiceProvider::class,
        Illuminate\Routing\ControllerServiceProvider::class,
        Illuminate\Cookie\CookieServiceProvider::class,
        Illuminate\Database\DatabaseServiceProvider::class,
        Illuminate\Encryption\EncryptionServiceProvider::class,
        Illuminate\Filesystem\FilesystemServiceProvider::class,
        Illuminate\Foundation\Providers\FoundationServiceProvider::class,
        Illuminate\Hashing\HashServiceProvider::class,
        Illuminate\Mail\MailServiceProvider::class,
        Illuminate\Pagination\PaginationServiceProvider::class,
        Illuminate\Pipeline\PipelineServiceProvider::class,
        Illuminate\Queue\QueueServiceProvider::class,
        Illuminate\Redis\RedisServiceProvider::class,
        Illuminate\Auth\Passwords\PasswordResetServiceProvider::class,
        Illuminate\Session\SessionServiceProvider::class,
        Illuminate\Translation\TranslationServiceProvider::class,
        Illuminate\Validation\ValidationServiceProvider::class,
        Illuminate\View\ViewServiceProvider::class,

        /*
         * Application Service Providers...
         */
        App\Providers\AppServiceProvider::class,
        App\Providers\AuthServiceProvider::class,
        App\Providers\EventServiceProvider::class,
        App\Providers\RouteServiceProvider::class,

		// Barryvdh\Debugbar\ServiceProvider::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Class Aliases
    |--------------------------------------------------------------------------
    |
    | This array of class aliases will be registered when this application
    | is started. However, feel free to register as many as you wish as
    | the aliases are "lazy" loaded so they don't hinder performance.
    |
    */

    'aliases' => [

        'App'       => Illuminate\Support\Facades\App::class,
        'Artisan'   => Illuminate\Support\Facades\Artisan::class,
        'Auth'      => Illuminate\Support\Facades\Auth::class,
        'Blade'     => Illuminate\Support\Facades\Blade::class,
        'Bus'       => Illuminate\Support\Facades\Bus::class,
        'Cache'     => Illuminate\Support\Facades\Cache::class,
        'Config'    => Illuminate\Support\Facades\Config::class,
        'Cookie'    => Illuminate\Support\Facades\Cookie::class,
        'Crypt'     => Illuminate\Support\Facades\Crypt::class,
        'DB'        => Illuminate\Support\Facades\DB::class,
        'Eloquent'  => Illuminate\Database\Eloquent\Model::class,
        'Event'     => Illuminate\Support\Facades\Event::class,
        'File'      => Illuminate\Support\Facades\File::class,
        'Gate'      => Illuminate\Support\Facades\Gate::class,
        'Hash'      => Illuminate\Support\Facades\Hash::class,
        'Input'     => Illuminate\Support\Facades\Input::class,
        'Inspiring' => Illuminate\Foundation\Inspiring::class,
        'Lang'      => Illuminate\Support\Facades\Lang::class,
        'Log'       => Illuminate\Support\Facades\Log::class,
        'Mail'      => Illuminate\Support\Facades\Mail::class,
        'Password'  => Illuminate\Support\Facades\Password::class,
        'Queue'     => Illuminate\Support\Facades\Queue::class,
        'Redirect'  => Illuminate\Support\Facades\Redirect::class,
        'Redis'     => Illuminate\Support\Facades\Redis::class,
        'Request'   => Illuminate\Support\Facades\Request::class,
        'Response'  => Illuminate\Support\Facades\Response::class,
        'Route'     => Illuminate\Support\Facades\Route::class,
        'Schema'    => Illuminate\Support\Facades\Schema::class,
        'Session'   => Illuminate\Support\Facades\Session::class,
        'Storage'   => Illuminate\Support\Facades\Storage::class,
        'URL'       => Illuminate\Support\Facades\URL::class,
        'Validator' => Illuminate\Support\Facades\Validator::class,
        'View'      => Illuminate\Support\Facades\View::class,

        // 'Debugbar' => Barryvdh\Debugbar\Facade::class,
    ],

];
