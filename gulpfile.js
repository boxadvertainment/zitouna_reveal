var gulp        = require('gulp');
var elixir      = require('laravel-elixir');
var browserSync = require('browser-sync').create('My server');
var dotenv      = require('dotenv').load();

var Task = elixir.Task;

elixir.extend('browserSync', function () {
    /* _____________________________________________________________________________________ */
    // Add these lines to node_modules/laravel-elixir/tasks/shared/Css.js
    // after .pipe(gulp.dest(options.output.baseDir))
    // =====================================================================================
    // .pipe(require('browser-sync').get('My server').stream({match: '**/*.css'}))
    /* _____________________________________________________________________________________ */

    var src = [
        'app/**/*',
        'public/**/*',
        'resources/views/**/*',
        '!public/css/*'
    ];

    gulp.task('serve', function () {
        browserSync.init({
            proxy: process.env.APP_URL
        });
    });

    new Task('browserSync', function() {
        if (browserSync.active === true) {
            browserSync.reload();
        }
    }).watch(src);
});

elixir(function(mix) {
    mix
        .rubySass('main.scss', 'public/css/main.css')
        .babel([
          'facebookUtils.js',
          'main.js'
        ], 'public/js/main.js')
        .scripts([
            'jquery/dist/jquery.min.js',
            'sweetalert/dist/sweetalert.min.js',
            'video.js/dist/video-js/video.js',
            'videojs-youtube/dist/vjs.youtube.js',
            'velocity/velocity.min.js',
            'velocity/velocity.ui.min.js',
            'bootstrap/dist/js/bootstrap.min.js',
            'animatesprite/scripts/jquery.animateSprite.min.js',
            'ionsound/js/ion.sound.min.js',
        ], 'public/js/vendor.js', 'vendor/bower_components' )
        .styles([
            'sweetalert/dist/sweetalert.css',
            'bootstrap/dist/css/bootstrap.min.css',
            'sweetalert/dist/sweetalert.css',
            'video.js/dist/video-js/video-js.min.css',
            'font-awesome/css/font-awesome.min.css',
        ], 'public/css/vendor.css', 'vendor/bower_components')
        .copy('vendor/bower_components/modernizr/modernizr.js', 'public/js/modernizr.js')
        .copy('vendor/bower_components/video.js/dist/video-js/video-js.swf', 'public/js/video-js.swf')
        .copy('vendor/bower_components/video.js/dist/video-js/font', 'public/font')
        .copy('vendor/bower_components/font-awesome/fonts', 'public/fonts')
        //.version(['css/main.css', 'js/main.js'])
        .browserSync();
});
